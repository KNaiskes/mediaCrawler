
#### A Social Media Web Crawler written in Python 3


### Supported Social Media Sites

* Facebook
* Twitter
* Flickr
* Reddit
* Google Plus
* Youtube

#### Setup

##### Install requirements
*pip3 install -r requirements.txt*

##### Tokens
Enter your tokens in *tokens.py* for each service

```
#Facebook credentials
FacebookToken = """"""

#Twitter credentials
Twitter_consumer_key = ""
Twitter_consumer_secret = "" 
Twitter_access_token = ""
Twitter_access_token_secret = "" 


#Flickr
Flickr_key = ""
Flickr_secret = ""

#G+
GooglePlus_key = ""

#Reddit

Reddit_client_id = ""
Reddit_client_secret = ""
Reddit_username = ""
Reddit_password = ""
Reddit_userAgent = ""

```

#### Screenshots of the project

![index](/uploads/da1281727c04fc29a1acfe6ab0c90d27/index.png)

![login](/uploads/5f6c859c8221bc5fed74795bba5d7fad/login.png)

![search](/uploads/7c5a688cc80defa5fad2ad3b5e5c8064/search.png)

![rsults1](/uploads/d478832cdf8f78e61e8ec39c3f81015c/rsults1.png)

![results2](/uploads/507d419ba202ca391319de131f03a68e/results2.png)

![results3](/uploads/3a7a76cbc3b672771f0cc3fe81f49740/results3.png)

![results4](/uploads/a415ae9cf2fb88eb47524e5046b5f0bf/results4.png)

![results6](/uploads/2c5d88c550e530aea77fbd422931735e/results6.png)

![results7](/uploads/948b3289e89a2dbe228c3a3db23d3e38/results7.png)


